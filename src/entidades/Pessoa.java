package entidades;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Pessoa {
	public static List<Pessoa> list = new ArrayList<Pessoa>();

	DecimalFormat formatador = new DecimalFormat("0.00");
	
	private String nome;
	private String sobrenome;
	private double peso;
	private double altura;
	
	public Pessoa() {
	}

	public Pessoa(String nome, String sobrenome, double peso, double altura) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.peso = peso;
		this.altura = altura;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", sobrenome=" + sobrenome + ", peso=" + peso + ", altura=" + altura + "]";
	}
	
	//Metodo para carregar os dados e tratar
	public static void carregar() {
		
				try (BufferedReader br = new BufferedReader(new FileReader("c:\\tabela.csv"))) {
								
			// Pulando primeira linha
			String line = br.readLine();
			line = br.readLine();
			
				while (line != null) {
					///Aqui usei o mtodo split que divide a linha lida em um array de String
					//passando como parametro o divisor ";"
					String[] vet = line.replaceAll(",",".").split(";");
					int a = vet.length;
					//Verificacao de posicao vazia
					if(a > 2) {
					
						//Atribuicao de valores
						String nome = vet[0];
						String sobrenome = vet[1];
						Double peso = Double.parseDouble(vet[2]);
						Double altura = Double.parseDouble(vet[3]);
						
						Pessoa pessoas = new Pessoa(nome, sobrenome, peso, altura);
						list.add(pessoas);
									
					}
					
					line = br.readLine();
				
			}
		
		}catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
			
	}
	//Metodo para a impressão dos resultados e gerar arquivo .txt
	public static void imprimir() throws IOException {
		
		FileWriter arq = new FileWriter("d:\\EduardoFerreiraDeOliveira.txt");
	    PrintWriter gravarArq = new PrintWriter(arq);
	    DecimalFormat formatador = new DecimalFormat("0.00");
	    
		//Impresso tela
		for(Pessoa pessoas : list ) {
			double imc = pessoas.getPeso()/(pessoas.getAltura()*pessoas.getAltura());
			System.out.println(pessoas.getNome().toUpperCase().replaceAll("\s", "") + " " + pessoas.getSobrenome().toUpperCase().replaceAll("( +)"," ").trim() + " " + formatador.format(imc) +"\n");
			
			//Gravao de arquivos em .txt
			gravarArq.printf(pessoas.getNome().toUpperCase().replaceAll("\s", "") + " " + pessoas.getSobrenome().toUpperCase().replaceAll("( +)"," ").trim() + " " + formatador.format(imc) +"\n");
	}
		arq.close();		
	}
	
}
